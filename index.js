var createGame = require('voxel-engine');
var highlight = require('voxel-highlight')
var player = require('voxel-player')
var world = require('./world.js');
var fly = require('voxel-fly')
var walk = require('voxel-walk')


var opts = {
    texturePath: './assets/textures/',
    // generate: world.generator,
    chunkSize: 32,
    chunkDistance: 2,
    //fogScale: 16,
    //lightsDisabled: true,
    generateChunks: false,
    materials: [
        ['grass', 'dirt', 'grass_dirt'], 'dirt', 'sand', 'cobblestone', 'ice', 'clouds', ['log_oak_top', 'log_oak_top', 'log_oak'], 'leaves_oak', ['whitewool', 'dirt', 'grass_dirt']
    ]
}

var game = createGame(opts);
var container = document.getElementById('container');
game.appendTo(container);

var createPlayer = player(game);
var dude = createPlayer('./assets/textures/shama.png');
dude.possess();
dude.yaw.position.set(0, 3, 0);

var generateChunk = world();
game.voxels.on('missingChunk', function (p) {
    var voxels = generateChunk(p, game.chunkSize)
    var chunk = {
        position: p,
        dims: [game.chunkSize, game.chunkSize, game.chunkSize],
        voxels: voxels
    }
    game.showChunk(chunk)
})

var makeFly = fly(game)
var target = game.controls.target()
game.flyer = makeFly(target)

game.on('tick', function() {
    walk.render(target.playerSkin)
    var vx = Math.abs(target.velocity.x)
    var vz = Math.abs(target.velocity.z)
    if (vx > 0.001 || vz > 0.001) walk.stopWalking()
    else walk.startWalking()
  })

//cloud
var clouds = require('voxel-clouds')({
    game: game,
    high: 16,
    distance: 300,
    many: 100,
    speed: 0.01,
    // material of the clouds
    material: new game.THREE.MeshBasicMaterial({
        emissive: 0x000000,
        shading: game.THREE.FlatShading,
        fog: false,
        transparent: true,
        opacity: 0.5,
    }),
});
game.on('tick', clouds.tick.bind(clouds));
//water
// var water = require('voxel-water')({
//     game: game,
//     high: 16,
//     distance: 300,
//     many: 100,
//     speed: 0.01,
//     // material of the clouds
//     material: new game.THREE.MeshBasicMaterial({
//         emissive: 0xffffff,
//         shading: game.THREE.FlatShading,
//         fog: false,
//         transparent: true,
//         opacity: 0.5,
//     }),
// });

window.addEventListener('keydown', function (ev) {
    if (ev.keyCode === 'T'.charCodeAt(0)) dude.toggle()
})

var highlighter = highlight(game, {adjacentActive:function () { return game.controls.state.firealt}});

container.addEventListener("mouseup", function (event) {
    var currentMaterial = 1
    if (event.button === 0) {
        var position = highlighter.currVoxelPos;
        if (position) {
            game.setBlock(position, 0);
        }
    } else if (event.button === 2) {
        var position = highlighter.currVoxelAdj;
        if (position) {
            game.createBlock(position, currentMaterial);
        }
    }
});
// var createSky = require('voxel-sky')(game);
// var sky = createSky(function(time) {
  
//     // spin the sky once per day
//     this.spin(Math.PI * 2 * (time / 2400));
  
//     // Change the sky pink at 7AM
//     if (time === 700) this.color(new this.game.THREE.Color(0xFF36AB), 1000);
  
//     if (time === 0) {
//       // paint a green square on the bottom (above your head at 1200)
//       this.paint('bottom', function() {
//         // The HTML canvas and context for each side are accessible
//         this.context.rect((this.canvas.width/2)-25, (this.canvas.height/2)-25, 50, 50);
//         this.context.fillStyle = this.rgba(0, 1, 0, 1);
//         this.context.fill();
//       });
  
//       // paint 500 stars on the top and bottom
//       this.paint(['top', 'bottom'], this.stars, 500);
  
//       // paint a big red full moon on every face
//       // 0 = full, 0.25 quarter waxing, 0.5 = new, 0.75 quarter waning
//       this.paint('all', this.moon, 0, 100, new this.game.THREE.Color(0xFF0000));
  
//       // paint a small green sun on each side face
//       this.paint('sides', this.sun, 10, new this.game.THREE.Color(0x00FF00));
//     }
  
//     if (time === 400) {
//       // clear the canvas on all sides at 4AM
//       this.paint('all', this.clear);
//     }
  
//     if (time === 1800) {
//       // lower the sunlight intensity at 6PM
//       this.sunlight.intensity = 0.2;
  
//       // set the ambient color (is a hemisphere light)
//       this.ambient.color.setHSL(0.9, 1, 1);
//     }
//   });
//   game.on('tick', sky);

// var createSky = require('voxel-sky')(game);
// var sky = createSky();
// game.on('tick', sky);

//voxel snow, material
var snow_flag = false;
var snow = require('voxel-snow')({
    game: game,
    count: 1000,
    size: 20
});
  
game.on('tick', function() {
    if(snow_flag) snow.tick();
    else snow.dead();
});

var rain_flag = false;
var rain = require('voxel-rain')({
    game: game,
    count: 3000,
    size: 10
});
  
game.on('tick', function() {
    if(rain_flag) rain.tick();
    else rain.dead();
});

window.addEventListener('keydown', function (ev) {
    if (ev.keyCode === 'M'.charCodeAt(0)) {
        if(snow_flag){
            game.materials.materials[0] = ["grass_dirt", "grass_dirt", "grass", "dirt", "grass_dirt", "grass_dirt"]
            snow_flag = false
        }else{
            game.materials.materials[0] = ["grass_dirt", "grass_dirt", "whitewool", "dirt", "grass_dirt", "grass_dirt"]
            snow_flag = true
        }
        game.showAllChunks();
        if(rain_flag) rain_flag = false
    }
    if (ev.keyCode === 'N'.charCodeAt(0)) {
        rain_flag = !rain_flag
        if(snow_flag) {
            snow_flag = false
            game.showAllChunks();
        }
    }
    if (ev.keyCode === 'P'.charCodeAt(0)) {
        console.log(game.materials)
    }
})